package com.cheng.library.base

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.cheng.library.action.ActivityAction
import com.cheng.library.action.BundleAction
import com.cheng.library.action.HandlerAction
import com.cheng.library.utils.StatusUtils

abstract class BaseActivity : AppCompatActivity(), HandlerAction, BundleAction, ActivityAction {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getLayoutId()?.let {
            setContentView(it)
        }
        setStatusColor()
        setSystemInvadeBlack()
        init(savedInstanceState)
    }

    /**
     * 获取Layout布局
     */
    abstract fun getLayoutId(): Int?

    /**
     * 入口，初始化
     */
    abstract fun init(savedInstanceState: Bundle?)

    /**
     * 设置状态栏背景颜色
     */
    open fun setStatusColor() {
        StatusUtils.setUseStatusBarColor(this, Color.parseColor("#ffffff"))
    }

    /**
     * 沉浸式状态
     */
    open fun setSystemInvadeBlack() {
        //第二个参数是是否沉浸,第三个参数是状态栏字体是否为黑色。
        StatusUtils.setSystemStatus(this, true, true)
    }

    override fun getBundle(): Bundle? = intent.extras

    override fun getContext(): Context  = this
}