package com.cheng.library.common

import android.content.Context
import android.graphics.Bitmap
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.MultiTransformation
import com.bumptech.glide.load.resource.bitmap.CenterInside
import com.bumptech.glide.load.resource.bitmap.FitCenter
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestOptions
import com.cheng.library.R
import com.cheng.library.widget.ZoomImageView
import de.hdodenhof.circleimageview.CircleImageView


fun AppCompatImageView.loadUrl(context: Context, url: String) {
    Glide.with(context)
        .load(url)
        .placeholder(R.drawable.img_loading)
        .error(R.drawable.img_error)
        .transition(withCrossFade())
        .into(this)
}

fun AppCompatImageView.loadBorder(context: Context, url: String) {
    Glide.with(context)
        .load(url)
        .into(this)
}

fun AppCompatImageView.loadDecoration(context: Context, url: String) {
    Glide.with(context)
        .load(url)
//        .apply(RequestOptions.bitmapTransform(MultiTransformation(FitCenter())))
        .into(this)
}

fun ImageView.loadUrl(context: Context, url: String) {
    Glide.with(context)
        .load(url)
        .placeholder(R.drawable.img_loading)
        .error(R.drawable.img_error)
        .into(this)
}

fun CircleImageView.loadUrl(context: Context, url: String) {
    Glide.with(context)
        .load(url)
        .placeholder(R.drawable.img_loading)
        .error(R.drawable.img_error)
        .into(this)
}

fun ZoomImageView.loadImage(context: Context, bitmap: Bitmap) {
    val mation = MultiTransformation<Bitmap>(CenterInside())
    Glide.with(context)
        .load(bitmap)
        .placeholder(R.drawable.img_loading)
        .error(R.drawable.img_error)
        .apply(RequestOptions.bitmapTransform(mation))
        .into(this)
}