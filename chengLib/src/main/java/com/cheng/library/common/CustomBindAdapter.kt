package com.cheng.library.common

import android.view.View
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter

object CustomBindAdapter {

    /**
     * 加载资源图片
     */
    @BindingAdapter(value = ["imgSrc"])
    @JvmStatic
    fun imgSrc(view: ImageView, id: Int) {
        view.setImageResource(id)
    }

    /**
     * 加载图片
     */
    @BindingAdapter(value = ["imageUrl"])
    @JvmStatic
    fun imageUrl(view: ImageView, url: String) {
        view.loadUrl(view.context.applicationContext, url)
    }

    @BindingAdapter(value = ["visible"])
    @JvmStatic
    fun visible(view: View, visible: Boolean) {
        view.visibility = if (visible) View.VISIBLE else View.GONE
    }

    /**
     * 加载资源图片
     */
    @BindingAdapter(value = ["drawableEnd"])
    @JvmStatic
    fun drawableEnd(view: AppCompatTextView, id: Int) {
        view.setCompoundDrawablesWithIntrinsicBounds(0, 0, id, 0)
    }

}