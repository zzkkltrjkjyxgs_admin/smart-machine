package com.cheng.library.common

import android.text.TextUtils
import androidx.annotation.StringRes
import com.hjq.toast.ToastUtils

fun toast(content: String) {
    if (TextUtils.isEmpty(content))
        return
    ToastUtils.show(content)
}

fun toast(@StringRes id: Int) {
    ToastUtils.show(id)
}







