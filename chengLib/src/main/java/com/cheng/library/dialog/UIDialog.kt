package com.cheng.library.dialog

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import com.cheng.library.R
import com.cheng.library.base.BaseDialog

/**
 * desc   : 项目通用 Dialog 布局封装
 */
class UIDialog {
    open class Builder(context: Context?) : BaseDialog.Builder<Builder>(context) {

        private var mContainerLayout: ViewGroup? = null
        private var mTitleView: TextView? = null
        private var mCancelView: TextView? = null
        private var mLineView: View? = null
        private var mConfirmView: TextView? = null
        private var mAutoDismiss = true

        init {
            setContentView(R.layout.dialog_ui)
            setAnimStyle(BaseDialog.ANIM_IOS)
            setGravity(Gravity.CENTER)

            mContainerLayout = findViewById(R.id.ll_ui_container)
            mTitleView = findViewById(R.id.tv_ui_title)
            mCancelView = findViewById(R.id.tv_ui_cancel)
            mLineView = findViewById(R.id.v_ui_line)
            mConfirmView = findViewById(R.id.tv_ui_confirm)
            setOnClickListener(mCancelView, mConfirmView)
            setOnClickListener(R.id.tv_ui_cancel, R.id.tv_ui_confirm)
        }

        fun setCustomView(@LayoutRes id: Int): Builder {
            return setCustomView(
                LayoutInflater.from(context).inflate(id, mContainerLayout, false)
            )
        }

        fun setCustomView(view: View?): Builder {
            mContainerLayout!!.addView(view, 1)
            return this
        }

        fun setTitle(@StringRes id: Int): Builder {
            return setTitle(getString(id))
        }

        fun setTitle(text: CharSequence?): Builder {
            mTitleView!!.text = text
            return this
        }

        fun setCancel(@StringRes id: Int): Builder {
            return setCancel(getString(id))
        }

        fun setCancel(text: CharSequence?): Builder {
            mCancelView!!.text = text
            mLineView!!.visibility =
                if (text == null || "" == text.toString()) View.GONE else View.VISIBLE
            return this
        }

        fun setConfirm(@StringRes id: Int): Builder {
            return setConfirm(getString(id))
        }

        fun setConfirm(text: CharSequence?): Builder {
            mConfirmView!!.text = text
            return this
        }

        fun setAutoDismiss(dismiss: Boolean): Builder {
            mAutoDismiss = dismiss
            return this
        }

        fun autoDismiss() {
            if (mAutoDismiss) {
                dismiss()
            }
        }
    }
}