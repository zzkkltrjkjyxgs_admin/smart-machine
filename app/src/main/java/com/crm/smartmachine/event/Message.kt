package com.crm.smartmachine.event

import java.text.SimpleDateFormat
import java.util.*

data class Message @JvmOverloads constructor(
    var code: Int = 0,
    var msg: String = "",
    var arg1: Int = 0,
    var arg2: Int = 0,
    var obj: Any? = null
){
    var time:String=SimpleDateFormat.getTimeInstance().format(Date())
}