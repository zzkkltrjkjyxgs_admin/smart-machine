package com.crm.smartmachine.network

/**
 * Name: HttpResult
 * Author: chenglifeng
 */
data class HttpResult<T>(val code: Int, val msg: String, val data: T) : IBaseResponse<T>{

    override fun code(): Int = code

    override fun msg(): String = msg

    override fun data(): T = data

    override fun isSuccess(): Boolean {
        if (code != 200)
            throw ResponseThrowable(this)
        return code == 200
    }
}