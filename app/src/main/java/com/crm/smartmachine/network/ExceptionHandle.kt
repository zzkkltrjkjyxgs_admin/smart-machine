package com.crm.smartmachine.network

import android.net.ParseException
import com.blankj.utilcode.util.AppUtils
import com.google.gson.JsonParseException
import com.google.gson.stream.MalformedJsonException
import org.json.JSONException
import retrofit2.HttpException
import java.net.ConnectException

/**
 * Name: ExceptionHandle
 * Author: chenglifeng
 */
object ExceptionHandle {

    fun handleException(e: Throwable): ResponseThrowable {
        if (AppUtils.isAppDebug()) {
            e.printStackTrace()
        }
        val ex: ResponseThrowable
        if (e is HttpException) {
            ex = if (e.code() == 401){
                ResponseThrowable(ERROR.AUTH_ERROR, e)
            } else {
                ResponseThrowable(ERROR.HTTP_ERROR, e)
            }
        } else if (e is JsonParseException
            || e is JSONException
            || e is ParseException || e is MalformedJsonException
        ) {
            ex = ResponseThrowable(ERROR.PARSE_ERROR, e)
        } else if (e is ConnectException) {
            ex = ResponseThrowable(ERROR.NETWORD_ERROR, e)
        } else if (e is javax.net.ssl.SSLException) {
            ex = ResponseThrowable(ERROR.SSL_ERROR, e)
        } else if (e is java.net.SocketTimeoutException) {
            ex = ResponseThrowable(ERROR.TIMEOUT_ERROR, e)
        } else if (e is java.net.UnknownHostException) {
            ex = ResponseThrowable(ERROR.TIMEOUT_ERROR, e)
        } else if (e is ResponseThrowable) {
            ex = e
        } else {
            ex = if (!e.message.isNullOrEmpty()) ResponseThrowable(1000, e.message!!, e)
            else ResponseThrowable(ERROR.UNKNOWN, e)
        }
        return ex
    }
}