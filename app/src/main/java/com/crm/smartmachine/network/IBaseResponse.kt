package com.crm.smartmachine.network

/**
 * Name: IBaseResponse
 * Author: chenglifeng
 */
interface IBaseResponse<T> {
    fun code(): Int
    fun msg(): String
    fun data(): T
    fun isSuccess(): Boolean
}