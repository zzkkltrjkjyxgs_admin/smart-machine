package com.crm.smartmachine.network

import java.lang.Exception

/**
 * Name: ResponseThrowable
 * Author: chenglifeng
 */
class ResponseThrowable: Exception {

    var code: Int
    var errMsg: String
    var data: String?

    constructor(error: ERROR, e: Throwable? = null) : super(e) {
        code = error.getKey()
        errMsg = error.getValue()
        data = error.getValue()
    }

    constructor(code: Int, msg: String, e: Throwable? = null) : super(e) {
        this.code = code
        this.errMsg = msg
        this.data = msg
    }

    constructor(base: IBaseResponse<*>, e: Throwable? = null) : super(e) {
        this.code = base.code()
        this.errMsg = base.msg()
        this.data = if(base.data() == null) null else base.data() as String
    }
}