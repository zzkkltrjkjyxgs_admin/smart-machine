package com.crm.smartmachine.contants

object Constants {
//    const val BASE_URL = "http://114.67.104.81:8887"
//    const val BASE_URL = "http://114.67.103.128:8887"
    const val BASE_URL = "http://122.114.14.148:14803"
    const val TOKEN = "token"
    const val LYJID = "A"
    const val SLAVEID = 1

    const val PAYLOAD_TV_GOODS_COUNT = 100
}

class EventConstants {
    companion object {
        const val BACK_HOME = "backHome" //返回首页
        const val UPDATE_HOME = "updateHome" //刷新首页
        const val UPDATE_BILL_COUNT = "updateBillCount"
        const val UPDATE_CASE_LIST = "updateCaseList"
        const val START_TIMER = "startTimer" //首页，开启轮询

        const val RESET_COUNT_DOWN = "resetCountDown" //重置返回首页的倒计时时间
        const val START_COUNT_DOWN = "startCountDown" //开始返回首页的倒计时
    }
}