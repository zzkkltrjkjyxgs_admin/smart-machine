package com.crm.smartmachine.contants

enum class ActionTypeEnum(val value: String) {

    SAVE("SAVE"), //存/取货
    SUPPLY("SUPPLY") //补货
}