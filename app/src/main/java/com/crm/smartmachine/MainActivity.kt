package com.crm.smartmachine

import android.os.Bundle
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.Fragment
import com.blankj.utilcode.util.LogUtils
import com.crm.smartmachine.base.SmartBaseActivity
import com.crm.smartmachine.contants.EventConstants
import com.crm.smartmachine.event.Message
import com.crm.smartmachine.ui.MainFragment
import com.crm.smartmachine.viewmodel.MainViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class MainActivity : SmartBaseActivity() {

    override fun getLayoutId(): Int? = R.layout.activity_main

    private val viewModel: MainViewModel by viewModel()

    private lateinit var tvCountDown: AppCompatTextView
    private lateinit var tvMachineNO: AppCompatTextView

    private val currentFragment by lazy {
        supportFragmentManager.findFragmentById(R.id.fragment_main)?.childFragmentManager?.primaryNavigationFragment
    }

    private var totalSecondsDuration = 310 //总时长310秒
    private var timer: Timer? = null

    override fun init(savedInstanceState: Bundle?) {

        tvCountDown = findViewById(R.id.tvCountDown)
        tvMachineNO = findViewById(R.id.tv_machine_no)

        tvMachineNO.text = "郑州市金水东路22号 (机器编号:NO.01000001)"


        observe()
    }

    private fun observe() {
        viewModel.defUI.msgEvent.observe(this) {
            when (it.msg) {
                EventConstants.START_COUNT_DOWN -> {
                    val current = it.obj as Fragment
                    LogUtils.a("开始" + current)
                    if (current !is MainFragment) {
                        timer?.cancel()
                        timer = Timer()
                        timer?.schedule(object : TimerTask() {
                            override fun run() {
                                runOnUiThread {
                                    totalSecondsDuration--
                                    if (totalSecondsDuration <= 0) {
                                        tvCountDown.text = ""
                                        viewModel.defUI.msgEvent.postValue(Message(0, EventConstants.BACK_HOME))
                                    } else if (totalSecondsDuration <= 300) {
                                        tvCountDown.text = "${totalSecondsDuration}秒后返回首页"
                                    } else {
                                        tvCountDown.text = ""
                                    }
                                }
                            }
                        }, 0, 1000)
                    }
                }
            }
        }
    }

//    override fun onTouchEvent(event: MotionEvent?): Boolean {
//        when(event?.action){
//            MotionEvent.ACTION_DOWN->{
//                LogUtils.a("重置时间：" + currentFragment)
//            }
//            MotionEvent.ACTION_UP->{
//                LogUtils.a("开始计时：" + currentFragment)
//            }
//        }
//        return super.onTouchEvent(event)
//    }

    override fun onUserInteraction() {
        super.onUserInteraction()
        totalSecondsDuration = 310
    }

    /**
     * 屏蔽侧滑返回
     */
    override fun onBackPressed() {
//        super.onBackPressed()
    }
}