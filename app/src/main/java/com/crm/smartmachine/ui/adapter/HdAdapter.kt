package com.crm.smartmachine.ui.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.crm.smartmachine.R
import com.crm.smartmachine.bean.response.HdBean

class HdAdapter : BaseQuickAdapter<HdBean, BaseViewHolder>(R.layout.item_hd) {

    override fun convert(holder: BaseViewHolder, item: HdBean) {
        item.apply {
            holder.setText(R.id.tv_sort_number, bh)
                .setText(R.id.tv_goods_name, mc)
                .setText(R.id.tv_goods_count, "库存数量：${kcsl ?: 0}")
        }
    }
}