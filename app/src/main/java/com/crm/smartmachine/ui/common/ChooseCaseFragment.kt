package com.crm.smartmachine.ui.common

import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import com.cheng.library.action.StatusAction
import com.cheng.library.common.clickNoRepeat
import com.cheng.library.common.toast
import com.cheng.library.utils.Param
import com.cheng.library.widget.HintLayout
import com.crm.smartmachine.R
import com.crm.smartmachine.base.SmartBaseFragment
import com.crm.smartmachine.contants.ActionTypeEnum
import com.crm.smartmachine.contants.Constants
import com.crm.smartmachine.contants.EventConstants
import com.crm.smartmachine.databinding.FragmentChooseCaseBinding
import com.crm.smartmachine.event.Message
import com.crm.smartmachine.manager.ModbusManager
import com.crm.smartmachine.manager.callback.WriteRegistersCallback
import com.crm.smartmachine.ui.common.adapter.ChooseCaseAdapter
import com.crm.smartmachine.ui.dialog.ChooseSaveTypeDialog
import com.crm.smartmachine.viewmodel.MainViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * 选择货道
 */
class ChooseCaseFragment : SmartBaseFragment<FragmentChooseCaseBinding>(), StatusAction {

    override fun getLayoutId(): Int? = R.layout.fragment_choose_case

//    private val viewModel: MainViewModel by viewModel()

    @Param
    private lateinit var action: String
    private val adapter by lazy { ChooseCaseAdapter() }

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)

        (binding.recyclerView.itemAnimator as DefaultItemAnimator).supportsChangeAnimations = false
        binding.recyclerView.adapter = adapter

        observe()
        onClick()

        //获取数据
        requestData()
    }

    private fun requestData() {
        //获取数据
        showLoading()
        if (action == ActionTypeEnum.SAVE.value) //存/取货
            viewModel.getSaveCaseList(Constants.LYJID)
        else //补货，接口9：获取补货货道列表
            viewModel.getSupplyCaseList()
    }

    private fun observe() {
        viewModel.defUI.msgEvent.observe(this) {
            when (it.msg) {
                EventConstants.BACK_HOME -> {
                    viewModel.defUI.msgEvent.postValue(Message(0, EventConstants.START_TIMER))
                    navigateBack()
                }
                EventConstants.UPDATE_CASE_LIST -> requestData()
            }
        }
        viewModel.errorResponse.observe(this) {
            handleErrorResponse(it)
            showEmpty()
        }
        viewModel.caseListResponse.observe(this) {
            showComplete()
            adapter.setList(it)
        }
    }

    private fun onClick() {
        //返回
        binding.actionBarBack.clickNoRepeat {
            viewModel.defUI.msgEvent.postValue(Message(0, EventConstants.START_TIMER))
            navigateBack()
        }
        //下一步
        binding.tvNextStep.clickNoRepeat {
            if (adapter.getSelectedPosition() == -1) {
                toast("请选择货道")
                return@clickNoRepeat
            }
            val bean = adapter.data[adapter.getSelectedPosition()]
            if (action == ActionTypeEnum.SAVE.value) {
                //选择存物品or审批单
                ChooseSaveTypeDialog.Builder(mContext)
                    .setListener(object : ChooseSaveTypeDialog.OnButtonClickListener {
                        override fun onLeftClick() {
                            ModbusManager.openCase4Save(bean.bh, object : WriteRegistersCallback {
                                override fun onSuccess() {
                                    navigate(R.id.action_chooseCaseFragment_to_chooseGoodsFragment,
                                        Bundle().apply {
//                                            putString("wpid", bean.unid)
                                            putString("wpbh", bean.wpbh ?: "")
                                            putString("kcsl", bean.kcsl ?: "0")
                                            putString("hdbh", bean.bh)
                                            putString("action", action)
                                        })
                                }
                            })
                        }

                        override fun onRightClick() {
                            ModbusManager.openCase4Save(bean.bh, object : WriteRegistersCallback {
                                override fun onSuccess() {
//                                    navigate(R.id.action_chooseCaseFragment_to_chooseSpdFragment,
//                                        Bundle().apply {
//                                            putString("wpbh", bean.wpbh)
//                                            putString("hdbh", bean.bh)
//                                            putString("action", action)
//                                        })
                                    navigate(R.id.action_chooseCaseFragment_to_chooseSpdIndexFragment,
                                        Bundle().apply {
                                            putString("action", action)
                                            putString("hdbh", bean.bh)
                                        })
                                }
                            })
                        }

                    })
                    .show()

            } else {
                navigate(R.id.action_chooseCaseFragment_to_chooseGoodsFragment, Bundle().apply {
//                    putString("wpid", bean.unid)
                    putString("wpbh", bean.wpbh ?: "")
                    putString("kcsl", bean.kcsl ?: "0")
                    putString("hdbh", bean.bh)
                    putString("action", action)
                })
            }
        }
    }

    override fun getHintLayout(): HintLayout = binding.hintLayout

}