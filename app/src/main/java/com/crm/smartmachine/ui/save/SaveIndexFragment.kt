package com.crm.smartmachine.ui.save

import android.os.Bundle
import android.view.View
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.listener.OnItemClickListener
import com.cheng.library.common.clickNoRepeat
import com.cheng.library.common.toast
import com.crm.smartmachine.R
import com.crm.smartmachine.base.SmartBaseFragment
import com.crm.smartmachine.bean.response.GoodsaaaBean
import com.crm.smartmachine.databinding.FragmentSaveIndexBinding
import com.crm.smartmachine.ui.save.adapter.SaveCupboardAdapter
import com.zgkxzx.modbus4And.requset.ModbusReq
import com.zgkxzx.modbus4And.requset.OnRequestBack

/**
 * 存货柜
 */
class SaveIndexFragment : SmartBaseFragment<FragmentSaveIndexBinding>(), OnItemClickListener {

    override fun getLayoutId(): Int? = R.layout.fragment_save_index

    private val adapter by lazy { SaveCupboardAdapter() }

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)

        binding.recyclerView.adapter = adapter
        adapter.setOnItemClickListener(this)

        observe()
        onClick()
        requestData()
    }

    private fun observe() {

    }

    private fun onClick() {
        //返回
        binding.actionBarBack.clickNoRepeat {
            navigateBack()
        }
    }

    override fun onItemClick(adapter: BaseQuickAdapter<*, *>, view: View, position: Int) {
        val boxPosition = (position + 1).toShort()
        ModbusReq.getInstance().writeRegisters(object : OnRequestBack<String> {
            override fun onSuccess(s: String) {
                toast("success")
            }

            override fun onFailed(msg: String) {
                toast("onFailed:$msg")
            }
        }, 1, 0, shortArrayOf(101, boxPosition, 3))
    }


    private fun requestData() {
        val data = ArrayList<GoodsaaaBean>()
        for (i in 1..35) {
            data.add(GoodsaaaBean("物品$i", (10..30).random()))
        }
        adapter.setList(data)
    }
}