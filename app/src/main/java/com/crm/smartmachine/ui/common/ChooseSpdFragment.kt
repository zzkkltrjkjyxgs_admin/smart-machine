package com.crm.smartmachine.ui.common

import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import com.cheng.library.action.StatusAction
import com.cheng.library.common.clickNoRepeat
import com.cheng.library.common.toast
import com.cheng.library.utils.Param
import com.cheng.library.widget.HintLayout
import com.crm.smartmachine.R
import com.crm.smartmachine.base.SmartBaseFragment
import com.crm.smartmachine.bean.request.SpdztRequest
import com.crm.smartmachine.bean.request.UpdateSpdLqwpWzRequest
import com.crm.smartmachine.bean.response.SPDBean
import com.crm.smartmachine.databinding.FragmentChooseSpdBinding
import com.crm.smartmachine.ui.common.adapter.ChooseSpdAdapter
import com.crm.smartmachine.viewmodel.MainViewModel
import com.qmuiteam.qmui.widget.dialog.QMUITipDialog
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * 选择审批单
 */
class ChooseSpdFragment : SmartBaseFragment<FragmentChooseSpdBinding>(), StatusAction {

    override fun getLayoutId(): Int? = R.layout.fragment_choose_spd

//    private val viewModel: MainViewModel by viewModel()

    @Param
    private lateinit var qhm: String

    @Param
    private lateinit var action: String

    @Param
    private lateinit var hdbh: String //货道编号

    private val adapter by lazy { ChooseSpdAdapter() }

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)

        (binding.recyclerView.itemAnimator as DefaultItemAnimator).supportsChangeAnimations = false
        binding.recyclerView.adapter = adapter

        binding.tvSelectedCase.text = "已选择：${hdbh}货道"

        observe()
        onClick()

        requestData()
    }

    private fun observe() {
        viewModel.errorResponse.observe(this) {
            handleErrorResponse(it)
            hideLoadingDialog()
        }
        //取货物品信息
        viewModel.getSpdByQhmResponse.observe(this) {
            hideLoadingDialog()
            adapter.setList(arrayListOf(it))
        }
        viewModel.updateSpdLqwpWzResponse.observe(this) {
            navigateSuccessFragment()
        }
    }

    private fun onClick() {
        //返回
        binding.actionBarBack.clickNoRepeat {
            navigateBack()
        }
        //存货
        binding.btnStart.clickNoRepeat {
            if (adapter.getSelectedPosition() == -1) {
                toast("请选择审批单")
                return@clickNoRepeat
            }
            val bean = adapter.data[adapter.getSelectedPosition()]
            //接口14：更新审批单领取物品位置
//            viewModel.updateSpdLqwpWz(UpdateSpdLqwpWzRequest(bean.unid, bean.wpwz))
            viewModel.updateSpdLqwpWz(UpdateSpdLqwpWzRequest(bean.unid, hdbh))
        }
    }

    private fun requestData() {
        showLoadingDialog()
        //接口3
        viewModel.getSpdByQhm(qhm, "Y")
    }

    private var loadingDialog: QMUITipDialog? = null

    private fun showLoadingDialog() {
        if (loadingDialog == null) {
            loadingDialog = QMUITipDialog
                .Builder(mContext)
                .setIconType(QMUITipDialog.Builder.ICON_TYPE_LOADING)
                .create()
        }
        loadingDialog?.show()
    }

    private fun hideLoadingDialog() {
        loadingDialog?.dismiss()
    }

    private fun navigateSuccessFragment() {
        navigate(R.id.action_chooseSpdFragment_to_handleGoodsSuccessFragment, Bundle().apply {
            putString("action", action)
        })
    }

    override fun getHintLayout(): HintLayout = binding.hintLayout

}