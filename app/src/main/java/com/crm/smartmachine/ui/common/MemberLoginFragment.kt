package com.crm.smartmachine.ui.common

import android.os.Bundle
import android.text.TextUtils
import com.blankj.utilcode.util.LogUtils
import com.cheng.library.common.clickNoRepeat
import com.cheng.library.common.toast
import com.cheng.library.utils.Param
import com.crm.smartmachine.R
import com.crm.smartmachine.base.SmartBaseFragment
import com.crm.smartmachine.contants.ActionTypeEnum
import com.crm.smartmachine.contants.EventConstants
import com.crm.smartmachine.databinding.FragmentMemberLoginBinding
import com.crm.smartmachine.event.Message
import com.crm.smartmachine.manager.ModbusManager
import com.crm.smartmachine.manager.callback.WriteRegistersCallback
import com.crm.smartmachine.viewmodel.MainViewModel
import com.qmuiteam.qmui.widget.dialog.QMUITipDialog
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * 存/补货员登录
 */
class MemberLoginFragment : SmartBaseFragment<FragmentMemberLoginBinding>() {

    override fun getLayoutId(): Int? = R.layout.fragment_member_login

//    private val viewModel: MainViewModel by viewModel()

    @Param
    private lateinit var action: String

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)

        binding.tvLoginTitle.text = if (action == ActionTypeEnum.SAVE.value) "存货员登录" else "补货员登录"

        observe()
        onClick()
    }

    private fun observe() {
        viewModel.defUI.msgEvent.observe(this) {
            when (it.msg) {
                EventConstants.BACK_HOME -> navigateBack()
            }
        }
        viewModel.errorResponse.observe(this) {
            hideLoadingDialog()
            toast(it.errMsg)
        }
        viewModel.bhyloginResponse.observe(this) {
            hideLoadingDialog()
            navigate(R.id.action_memberLoginFragment_to_chooseCaseFragment, Bundle().apply {
                putString("action", action)
            })
        }
    }

    private fun onClick() {
        //返回
        binding.actionBarBack.clickNoRepeat {
            viewModel.defUI.msgEvent.postValue(Message(0, EventConstants.START_TIMER))
            navigateBack()
        }
        //登录
        binding.btnSubmit.clickNoRepeat {
            val account = binding.etAccount.text.toString().trim()
            val password = binding.etPassword.text.toString().trim()
            if (TextUtils.isEmpty(account)) {
                toast("请输入账号")
                return@clickNoRepeat
            }
            if (TextUtils.isEmpty(password)) {
                toast("请输入密码")
                return@clickNoRepeat
            }
            hideSoftInput()
            showLoadingDialog()
            //接口5：补货员登录
            viewModel.bhylogin(account, password)
        }
    }

    private var loadingDialog: QMUITipDialog? = null

    private fun showLoadingDialog() {
        if (loadingDialog == null) {
            loadingDialog = QMUITipDialog
                .Builder(mContext)
                .setIconType(QMUITipDialog.Builder.ICON_TYPE_LOADING)
                .create()
        }
        loadingDialog?.show()
    }

    private fun hideLoadingDialog() {
        loadingDialog?.dismiss()
    }

}