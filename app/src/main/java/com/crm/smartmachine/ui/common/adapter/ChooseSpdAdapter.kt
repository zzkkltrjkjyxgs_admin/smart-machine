package com.crm.smartmachine.ui.common.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.crm.smartmachine.R
import com.crm.smartmachine.bean.response.CHDBean
import com.crm.smartmachine.bean.response.QhBean
import com.crm.smartmachine.bean.response.SPDBean

class ChooseSpdAdapter : BaseQuickAdapter<QhBean, BaseViewHolder>(R.layout.item_choose_spd) {

    private var selectedPosition = -1
    private var lastPosition = -1

    override fun convert(holder: BaseViewHolder, item: QhBean) {
        item.apply {

//            val wp = slwpList.map { it.wpmc }.toString()

            holder.setText(R.id.tvName, djbh)
                .setText(R.id.tvSpec, lyr)
                .setText(R.id.tvWp, qhm)
        }

        holder.itemView.isSelected = holder.layoutPosition == selectedPosition
        val checkIcon =
            if (holder.itemView.isSelected) R.drawable.icon_checkbox_checked else R.drawable.icon_checkbox_normal
        holder.setImageResource(R.id.ivCheckIcon, checkIcon)

        holder.itemView.setOnClickListener {
            holder.itemView.isSelected = true
            lastPosition = selectedPosition
            selectedPosition = holder.layoutPosition
            holder.setImageResource(R.id.ivCheckIcon, R.drawable.icon_checkbox_checked)
            notifyItemChanged(lastPosition)
        }
    }

    interface OnSelectAllListener {
        fun onResult(all: Boolean)
    }

    fun getSelectedPosition(): Int {
        return selectedPosition
    }

    fun setSelectedPosition(position: Int) {
        selectedPosition = position
        notifyDataSetChanged()
    }

}