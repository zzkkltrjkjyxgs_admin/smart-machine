package com.crm.smartmachine.ui.receive.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.crm.smartmachine.R
import com.crm.smartmachine.bean.response.SPDBean

class BillAdapter : BaseQuickAdapter<SPDBean, BaseViewHolder>(R.layout.item_ling_yong_dan_ju) {

    override fun convert(holder: BaseViewHolder, item: SPDBean) {

        item.apply {
            val textColor = if ("是" == sfylq) R.color.text_color_sub else R.color.text_color
            holder.setText(R.id.tv_no, wpbh)
                .setTextColorRes(R.id.tv_no, textColor)
                .setText(R.id.tv_goods_name, wpmc)
                .setTextColorRes(R.id.tv_goods_name, textColor)
                .setText(R.id.tv_old_count, kcsl.toString())
                .setTextColorRes(R.id.tv_old_count, textColor)
                .setText(R.id.tv_receive_count, slsl.toString())
                .setTextColorRes(R.id.tv_receive_count, textColor)
        }

    }

}