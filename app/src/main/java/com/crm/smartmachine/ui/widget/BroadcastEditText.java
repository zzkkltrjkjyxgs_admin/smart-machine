package com.crm.smartmachine.ui.widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.text.Editable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import androidx.appcompat.widget.AppCompatEditText;

import com.crm.smartmachine.R;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class BroadcastEditText extends AppCompatEditText {
    EtBroadcastReceiver receiver = new EtBroadcastReceiver();
    Context mContext = null;
    boolean focuse = false;
    InputMethodManager mInputMethodManager;
    String action = "mykeyboard";

    public BroadcastEditText(Context context) {
        this(context, null);
    }

    public BroadcastEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.broadcast_et);
        action = a.getString(R.styleable.broadcast_et_action);
        if (TextUtils.isEmpty(action)) {
            action = "mykeyboard";
        }

        mContext = context;
        mContext.registerReceiver(receiver, new IntentFilter(action));
        mInputMethodManager = ((InputMethodManager) this.mContext.getSystemService(Context.INPUT_METHOD_SERVICE));
//        setInputType(InputType.TYPE_NULL);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mContext.unregisterReceiver(receiver);
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {

        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        this.focuse = focused;
        if (focused) {
            hideSystemKeyboard();
        }else{
            Log.d("失去焦点","否出"+this.toString());
        }
    }

    private void onInput(String s) {

        if (focuse) {
            if (s.equals("del")) {
                int index = getSelectionStart();
                Log.d("del", "onInput: $index" + index);
                if (index > 0) {
                    Editable editable = getText();
                    editable.delete(index - 1, index);
                }
            } else if (s.equals("clear")) {
                int index = getSelectionStart();
                Log.d("clear", "onInput: $index" + index);
                if (index > 0) {
                    Editable editable = getText();
                    editable.delete(0, index);
                }
            } else if (s.equals("ok")) {

            } else {
                Editable et = getText();
                et.append(s);
            }
        }

    }

    protected class EtBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (action.equals(intent.getAction())) {
                String code = intent.getExtras().getString("code");
                onInput(code);
            }
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    /**
     * 隐藏输入软键盘
     *
     * @param context
     * @param view
     */
    public void hideInputManager(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (view != null && imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0); //强制隐藏
        }
    }

    /**
     * 隐藏系统的软键盘
     */
    public void hideSystemKeyboard() {
        Method localMethod = null;
        try {
            localMethod = EditText.class.getMethod("setShowSoftInputOnFocus", Boolean.TYPE);
            localMethod.setAccessible(true);
            try {
                localMethod.invoke(this, Boolean.FALSE);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            mInputMethodManager.hideSoftInputFromWindow(this.getWindowToken(), 0);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

    }

}
