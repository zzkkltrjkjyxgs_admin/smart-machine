package com.crm.smartmachine.ui.supply

import android.os.Bundle
import com.cheng.library.common.clickNoRepeat
import com.cheng.library.common.toast
import com.crm.smartmachine.R
import com.crm.smartmachine.base.SmartBaseFragment
import com.crm.smartmachine.databinding.FragmentSupplyIndexBinding

/**
 * 补货
 */
class SupplyIndexFragment : SmartBaseFragment<FragmentSupplyIndexBinding>() {

    override fun getLayoutId(): Int? = R.layout.fragment_supply_index

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)

        observe()
        onClick()
    }

    private fun observe() {

    }

    private fun onClick() {
        //返回
        binding.actionBarBack.clickNoRepeat {
            navigateBack()
        }
    }
}