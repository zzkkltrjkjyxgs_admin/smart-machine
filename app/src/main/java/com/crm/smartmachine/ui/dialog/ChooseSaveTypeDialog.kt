package com.crm.smartmachine.ui.dialog

import android.content.Context
import android.view.Gravity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatImageView
import com.cheng.library.base.BaseDialog
import com.cheng.library.common.clickNoRepeat
import com.crm.smartmachine.R

class ChooseSaveTypeDialog {
    class Builder(context: Context?) : BaseDialog.Builder<Builder>(context) {

        private var listener: OnButtonClickListener? = null
        private var ivClose: AppCompatImageView
        private var btnLeft: AppCompatButton
        private var btnRight: AppCompatButton

        init {
            setContentView(R.layout.dialog_choose_save_type)
            setAnimStyle(BaseDialog.ANIM_IOS)
            setGravity(Gravity.CENTER)
            setCancelable(false)
            setCanceledOnTouchOutside(false)

            ivClose = findViewById(R.id.ivClose)
            btnLeft = findViewById(R.id.btnLeft)
            btnRight = findViewById(R.id.btnRight)

            ivClose.clickNoRepeat {
                dismiss()
            }
            btnLeft.clickNoRepeat {
                dismiss()
                listener?.onLeftClick()
            }
            btnRight.clickNoRepeat {
                dismiss()
                listener?.onRightClick()
            }
        }

        fun setListener(listener: OnButtonClickListener): Builder {
            this.listener = listener
            return this
        }
    }

    interface OnButtonClickListener {
        fun onLeftClick()
        fun onRightClick()
    }
}