package com.crm.smartmachine.ui

import android.os.Bundle
import com.cheng.library.common.clickNoRepeat
import com.cheng.library.common.toast
import com.crm.smartmachine.R
import com.crm.smartmachine.base.SmartBaseFragment
import com.crm.smartmachine.databinding.FragmentLoginBinding
import com.crm.smartmachine.databinding.FragmentSupplyIndexBinding

/**
 * 补货员登录
 */
class LoginFragment : SmartBaseFragment<FragmentLoginBinding>() {

    override fun getLayoutId(): Int? = R.layout.fragment_login

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)

        observe()
        onClick()
    }

    private fun observe() {

    }

    private fun onClick() {
        //返回
        binding.actionBarBack.clickNoRepeat {
            navigateBack()
        }
        //登录
        binding.btnSubmit.clickNoRepeat {
            val account = binding.etAccount.text.toString().trim()
            val password = binding.etPassword.text.toString().trim()
            toast("account:$account;password:$password")
            navigate(R.id.action_loginFragment_to_supplyIndexFragment)
        }
    }
}