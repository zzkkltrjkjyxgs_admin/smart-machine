package com.crm.smartmachine.ui.receive.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.crm.smartmachine.R
import com.crm.smartmachine.bean.response.ReceiveBillBean
import com.crm.smartmachine.bean.response.SPDBean
import com.crm.smartmachine.contants.EventConstants
import com.crm.smartmachine.event.Message
import com.yibankeji.mask.event.LiveBus

class BillAdapter2 : BaseQuickAdapter<SPDBean, BaseViewHolder>(R.layout.item_receive_bill) {

    private var selectedPositionList = ArrayList<Int>()
    private var lastPosition = -1

    override fun convert(holder: BaseViewHolder, item: SPDBean) {

        item.apply {
            val checkIcon = if ("是" == sfylq) R.drawable.icon_checkbox_disable else R.drawable.icon_checkbox_normal
            val textColor = if ("是" == sfylq) R.color.text_color_sub else R.color.text_color
            holder.setImageResource(R.id.ivCheckIcon, checkIcon)
                .setText(R.id.tv_no, wpbh)
                .setTextColorRes(R.id.tv_no,textColor)
                .setText(R.id.tv_goods_name, wpmc)
                .setTextColorRes(R.id.tv_goods_name,textColor)
                .setText(R.id.tv_old_count, kcsl.toString())
                .setTextColorRes(R.id.tv_old_count,textColor)
                .setText(R.id.tv_receive_count, slsl.toString())
                .setTextColorRes(R.id.tv_receive_count,textColor)
        }

        if ("是" == item.sfylq) {
            holder.setImageResource(R.id.ivCheckIcon, R.drawable.icon_checkbox_disable)
            holder.itemView.setOnClickListener {}
        } else {
            val checkIcon =
                if (holder.layoutPosition in selectedPositionList) R.drawable.icon_checkbox_checked else R.drawable.icon_checkbox_normal
            holder.setImageResource(R.id.ivCheckIcon, checkIcon)
            holder.itemView.setOnClickListener {
                if (holder.layoutPosition in selectedPositionList) {
                    selectedPositionList.remove(holder.layoutPosition)
                } else {
                    selectedPositionList.add(holder.layoutPosition)
                }
                lastPosition = holder.layoutPosition
                notifyItemChanged(lastPosition)
                LiveBus.getInstance().with(Message::class.java)
                    .postValue(Message(0, EventConstants.UPDATE_BILL_COUNT))
            }
        }
    }

    fun selectAll(listener: OnSelectAllListener) {
        if (selectedPositionList.isEmpty()) {
            for ((index, item) in data.withIndex()) {
                if ("是" != item.sfylq) {
                    selectedPositionList.add(index)
                }
            }
        } else {
            selectedPositionList.clear()
        }
        listener.onResult(selectedPositionList.isNotEmpty())
        notifyDataSetChanged()
    }

    interface OnSelectAllListener {
        fun onResult(all: Boolean)
    }

    fun getSelectedPosition(): ArrayList<Int> {
        return selectedPositionList
    }

    fun clearSelectedPosition(){
        selectedPositionList.clear()
    }
}