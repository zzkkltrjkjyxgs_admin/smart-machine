package com.crm.smartmachine.ui.receive

import android.graphics.Rect
import android.os.Bundle
import android.text.TextUtils
import android.widget.FrameLayout
import android.widget.LinearLayout
import com.blankj.utilcode.util.LogUtils
import com.cheng.library.common.clickNoRepeat
import com.cheng.library.common.toast
import com.crm.smartmachine.R
import com.crm.smartmachine.base.SmartBaseFragment
import com.crm.smartmachine.contants.EventConstants
import com.crm.smartmachine.databinding.FragmentReceiveIndexBinding
import com.crm.smartmachine.event.Message
import com.crm.smartmachine.viewmodel.MainViewModel
import com.huawei.hms.hmsscankit.RemoteView
import com.huawei.hms.ml.scan.HmsScan
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * 领用
 */
class ReceiveIndexFragment : SmartBaseFragment<FragmentReceiveIndexBinding>() {

    override fun getLayoutId(): Int? = R.layout.fragment_receive_index

//    private val viewModel: MainViewModel by viewModel()

    private var remoteView: RemoteView? = null
    val SCAN_FRAME_SIZE = 200
    val SCAN_TOP = 100

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)

        val dm = resources.displayMetrics
        val density = dm.density
        var mScreenWidth = resources.displayMetrics.widthPixels
        var mScreenHeight = resources.displayMetrics.heightPixels
        val scanFrameSize = (SCAN_FRAME_SIZE * density).toInt()

        val scanTop = (SCAN_TOP * density).toInt()

        val rect = Rect()
        rect.left = mScreenWidth / 2 - scanFrameSize / 2
        rect.right = mScreenWidth / 2 + scanFrameSize / 2
//        rect.top = mScreenHeight / 2 - scanFrameSize / 2
//        rect.bottom = mScreenHeight / 2 + scanFrameSize / 2
        rect.top = scanTop
        rect.bottom = scanTop + scanFrameSize

        remoteView = RemoteView.Builder()
            .setContext(activity)
            .setBoundingBox(rect)
//            .setContinuouslyScan(false)
            .setFormat(HmsScan.ALL_SCAN_TYPE)
            .build()
        remoteView?.onCreate(savedInstanceState)
        val params = FrameLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        binding.rim.addView(remoteView, params)

        binding.etPickUpCode.requestFocus()
        binding.vm = viewModel

        observe()
        onClick()
    }

    private fun observe() {
        viewModel.defUI.msgEvent.observe(this) {
            when (it.msg) {
                EventConstants.BACK_HOME -> {
                    viewModel.defUI.msgEvent.postValue(Message(0, EventConstants.START_TIMER))
                    navigateBack()
                }
            }
        }

        remoteView?.setOnResultCallback {
            if (it != null && it.isNotEmpty() && it[0] != null && !TextUtils.isEmpty(it[0].getOriginalValue())) {
                LogUtils.a("result: ${it[0].getOriginalValue()}")
                navigate(R.id.action_receiveIndexFragment_to_receiveBillFragment, Bundle().apply {
                    putString("qhm", it[0].getOriginalValue())
                })
            }
        }
    }

    private fun onClick() {
        //返回
        binding.actionBarBack.clickNoRepeat {
            viewModel.defUI.msgEvent.postValue(Message(0, EventConstants.START_TIMER))
            navigateBack()
        }
        binding.btnConfirm.clickNoRepeat {
            val code = binding.etPickUpCode.text.toString().trim()
            if (TextUtils.isEmpty(code)){
                toast("请输入取货码")
                return@clickNoRepeat
            }
            navigate(R.id.action_receiveIndexFragment_to_receiveBillFragment, Bundle().apply {
                putString("qhm",code)
            })
        }
    }

    override fun onStart() {
        super.onStart()
        remoteView?.onStart()
    }

    override fun onResume() {
        super.onResume()
        remoteView?.onResume()
    }

    override fun onPause() {
        super.onPause()
        remoteView?.onPause()
    }

    override fun onStop() {
        super.onStop()
        remoteView?.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        remoteView?.onDestroy()
    }
}