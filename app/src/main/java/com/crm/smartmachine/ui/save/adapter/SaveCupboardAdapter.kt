package com.crm.smartmachine.ui.save.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.crm.smartmachine.R
import com.crm.smartmachine.bean.response.GoodsaaaBean

class SaveCupboardAdapter : BaseQuickAdapter<GoodsaaaBean, BaseViewHolder>(R.layout.item_cupboard) {

    override fun convert(holder: BaseViewHolder, item: GoodsaaaBean) {
        holder.setText(R.id.tv_sort_number, (holder.layoutPosition + 1).toString())
            .setText(R.id.tv_goods_name, item.goodsName)
            .setText(R.id.tv_goods_count, "数量：${item.goodsCount}")
    }
}