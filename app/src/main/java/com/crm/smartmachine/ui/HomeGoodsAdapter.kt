package com.crm.smartmachine.ui

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.crm.smartmachine.R
import com.crm.smartmachine.bean.response.HdBean
import com.crm.smartmachine.bean.response.LYJBean

class HomeGoodsAdapter : BaseQuickAdapter<HdBean, BaseViewHolder>(R.layout.item_home_goods) {

    override fun convert(holder: BaseViewHolder, item: HdBean) {
        item.apply {
            holder.setText(R.id.tv_sort_number,"货道编号:${bh}")
                .setText(R.id.tv_goods_name, mc)
                .setText(R.id.tv_goods_count, "库存数量：${kcsl?:0}")
        }
    }
}