package com.crm.smartmachine.ui.common.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.crm.smartmachine.R
import com.crm.smartmachine.bean.response.HdBean

class ChooseCaseAdapter : BaseQuickAdapter<HdBean, BaseViewHolder>(R.layout.item_case) {

    private var selectedPosition = -1
    private var lastPosition = -1

    override fun convert(holder: BaseViewHolder, item: HdBean) {
        item.apply {
            holder.setText(R.id.tvSortNumber, bh)
                .setText(R.id.tvName, mc)
                .setText(R.id.tvCount, "数量：${kcsl ?: 0}")
        }

        holder.itemView.isSelected = holder.layoutPosition == selectedPosition
        holder.itemView.setOnClickListener {
            holder.itemView.isSelected = true
            lastPosition = selectedPosition
            selectedPosition = holder.layoutPosition
            notifyItemChanged(lastPosition)
        }
    }

    fun getSelectedPosition(): Int {
        return selectedPosition
    }
}