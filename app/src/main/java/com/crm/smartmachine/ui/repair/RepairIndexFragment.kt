package com.crm.smartmachine.ui.repair

import android.os.Bundle
import android.text.TextUtils
import com.cheng.library.common.clickNoRepeat
import com.cheng.library.common.toast
import com.crm.smartmachine.R
import com.crm.smartmachine.base.SmartBaseFragment
import com.crm.smartmachine.bean.request.BxRequest
import com.crm.smartmachine.contants.Constants
import com.crm.smartmachine.contants.EventConstants
import com.crm.smartmachine.databinding.FragmentRepairIndexBinding
import com.crm.smartmachine.event.Message
import com.crm.smartmachine.viewmodel.MainViewModel
import com.qmuiteam.qmui.widget.dialog.QMUITipDialog
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * 报修
 */
class RepairIndexFragment : SmartBaseFragment<FragmentRepairIndexBinding>() {

    override fun getLayoutId(): Int? = R.layout.fragment_repair_index

//    private val viewModel: MainViewModel by viewModel()

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)

        observe()
        onClick()
    }

    private fun observe() {
        viewModel.defUI.msgEvent.observe(this) {
            when (it.msg) {
                EventConstants.BACK_HOME -> navigateBack()
            }
        }
        viewModel.errorResponse.observe(this) {
            handleErrorResponse(it)
            hideLoadingDialog()
        }
        viewModel.bxResponse.observe(this) {
            hideLoadingDialog()
            toast(it)
            viewModel.defUI.msgEvent.postValue(Message(0, EventConstants.START_TIMER))
            navigateBack()
        }
    }

    private fun onClick() {
        //返回
        binding.actionBarBack.clickNoRepeat {
            viewModel.defUI.msgEvent.postValue(Message(0, EventConstants.START_TIMER))
            navigateBack()
        }
        //提交
        binding.btnSubmit.clickNoRepeat {
            val bxnr = binding.etRepairContent.text.toString().trim()
            if (TextUtils.isEmpty(bxnr)) {
                toast("请输入报修内容")
                return@clickNoRepeat
            }
            val bxr = binding.etRepairMember.text.toString().trim()
            if (TextUtils.isEmpty(bxr)) {
                toast("请输入报修人")
                return@clickNoRepeat
            }
            val lxdh = binding.etContactType.text.toString().trim()
            if (TextUtils.isEmpty(lxdh)) {
                toast("请输入联系方式")
                return@clickNoRepeat
            }
            hideSoftInput()
            showLoadingDialog()
            //接口12：报修记录
            viewModel.bx(BxRequest(Constants.LYJID, bxnr, bxr, lxdh))
        }
    }

    private var loadingDialog: QMUITipDialog? = null

    private fun showLoadingDialog() {
        if (loadingDialog == null) {
            loadingDialog = QMUITipDialog
                .Builder(mContext)
                .setIconType(QMUITipDialog.Builder.ICON_TYPE_LOADING)
                .create()
        }
        loadingDialog?.show()
    }

    private fun hideLoadingDialog() {
        loadingDialog?.dismiss()
    }
}