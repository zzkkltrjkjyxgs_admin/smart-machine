package com.crm.smartmachine.ui.common

import android.os.Bundle
import android.os.CountDownTimer
import com.cheng.library.common.clickNoRepeat
import com.cheng.library.utils.Param
import com.crm.smartmachine.R
import com.crm.smartmachine.base.SmartBaseFragment
import com.crm.smartmachine.contants.ActionTypeEnum
import com.crm.smartmachine.contants.EventConstants
import com.crm.smartmachine.databinding.FragmentHandleGoodsSuccessBinding
import com.crm.smartmachine.event.Message
import com.crm.smartmachine.viewmodel.MainViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * 存/取货、补货成功
 */
class HandleGoodsSuccessFragment : SmartBaseFragment<FragmentHandleGoodsSuccessBinding>() {

    override fun getLayoutId(): Int? = R.layout.fragment_handle_goods_success

//    private val viewModel: MainViewModel by viewModel()

    @Param
    private lateinit var action: String
    private var countDownTimer: CountDownTimer?=null

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)

        countDownTimer = null
        countDownTimer = object : CountDownTimer(11000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                binding.btnBackHome.text = "返回首页(${millisUntilFinished / 1000}s)"
            }

            override fun onFinish() {
                binding.btnBackHome.text = "返回首页"
                navigateBackHome()
            }
        }
        binding.tvState.text = if (action == ActionTypeEnum.SAVE.value) "完成存/取货！" else "成功补货！"
        binding.btnContinue.text = if (action == ActionTypeEnum.SAVE.value) "继续存/取货！" else "继续补货！"
        postDelayed({
            countDownTimer?.start()
        }, 800)
        observe()
        onClick()
    }

    private fun observe() {

    }

    private fun onClick() {
        //继续
        binding.btnContinue.clickNoRepeat {
            navigateBack()
            viewModel.defUI.msgEvent.postValue(Message(0, EventConstants.UPDATE_CASE_LIST))
        }
        //返回首页
        binding.btnBackHome.clickNoRepeat {
            navigateBackHome()
        }
    }

    private fun navigateBackHome() {
        navigateBack()
        viewModel.defUI.msgEvent.postValue(Message(0, EventConstants.BACK_HOME))
    }

    override fun onDestroyView() {
        countDownTimer?.cancel()
        countDownTimer = null
        super.onDestroyView()
    }
}