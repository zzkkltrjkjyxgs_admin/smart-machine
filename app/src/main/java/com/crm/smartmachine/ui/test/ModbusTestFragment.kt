package com.crm.smartmachine.ui.test

import android.os.Bundle
import android.text.TextUtils
import com.cheng.library.common.clickNoRepeat
import com.cheng.library.common.toast
import com.crm.smartmachine.R
import com.crm.smartmachine.base.SmartBaseFragment
import com.crm.smartmachine.databinding.FragmentModbusTestBinding
import com.zgkxzx.modbus4And.requset.ModbusParam
import com.zgkxzx.modbus4And.requset.ModbusReq
import com.zgkxzx.modbus4And.requset.OnRequestBack
import java.util.*

class ModbusTestFragment : SmartBaseFragment<FragmentModbusTestBinding>() {

    override fun getLayoutId(): Int? = R.layout.fragment_modbus_test

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)

        //初始化
        binding.btnConfirm.clickNoRepeat {
            val host = binding.etIpAddress.text.toString().trim()
            if (TextUtils.isEmpty(host)) {
                toast("请输入ip")
                return@clickNoRepeat
            }
            val port = binding.etPort.text.toString().trim()
            ModbusReq.getInstance()
                .setParam(
                    ModbusParam()
                        .setHost(host) //192.168.3.190
                        .setPort(if (TextUtils.isEmpty(port)) 502 else Integer.parseInt(port))
                        .setEncapsulated(false)
                        .setKeepAlive(true)
                        .setTimeout(2000)
                        .setRetries(0)
                )
                .init(object : OnRequestBack<String> {
                    override fun onSuccess(p0: String?) {
                        toast("初始化成功")
                    }

                    override fun onFailed(p0: String?) {
                        toast("初始化失败: $p0")
                    }
                })
        }

        binding.btnWriteMultipleValue.clickNoRepeat {
            val inputSlaveId = binding.etSlaveId.text.toString().trim()
            val slaveId = if (TextUtils.isEmpty(inputSlaveId)) 1 else Integer.parseInt(inputSlaveId)
            ModbusReq.getInstance().writeRegisters(object : OnRequestBack<String> {
                override fun onSuccess(s: String) {
                    toast("success")
                }

                override fun onFailed(msg: String) {
                    toast("onFailed:$msg")
                }
            }, slaveId, 0, shortArrayOf(100, 1, 3))
        }

    }

    override fun onDestroy() {
        ModbusReq.getInstance().destory()
        super.onDestroy()
    }

    private fun onClick() {
//        binding.btnReadCoil.clickNoRepeat {
//            ModbusReq.getInstance().readCoil(object : OnRequestBack<BooleanArray?> {
//                override fun onSuccess(booleen: BooleanArray?) {
//                    LogUtils.d("readCoil onSuccess:${Arrays.toString(booleen)}")
//                }
//
//                override fun onFailed(msg: String) {
//                    LogUtils.d("readCoil onFailed:$msg")
//                }
//            }, 1, 1, 2)
//        }
//        binding.btnReadDiscreteInput.clickNoRepeat {
//            ModbusReq.getInstance().readDiscreteInput(object : OnRequestBack<BooleanArray?> {
//                override fun onSuccess(booleen: BooleanArray?) {
//                    LogUtils.d("readDiscreteInput onSuccess:${Arrays.toString(booleen)}")
//                }
//
//                override fun onFailed(msg: String) {
//                    LogUtils.d("readDiscreteInput onFailed $msg")
//                }
//            }, 1, 1, 5)
//        }
//        binding.btnReadHoldingRegisters.clickNoRepeat {
//            ModbusReq.getInstance().readHoldingRegisters(object : OnRequestBack<ShortArray?> {
//                override fun onSuccess(data: ShortArray?) {
//                    LogUtils.d("readHoldingRegisters onSuccess " + Arrays.toString(data))
//                }
//
//                override fun onFailed(msg: String) {
//                    LogUtils.d("readHoldingRegisters onFailed $msg")
//                }
//            }, 1, 2, 8)
//        }
//        binding.btnReadInputRegisters.clickNoRepeat {
//            ModbusReq.getInstance().readInputRegisters(object : OnRequestBack<ShortArray?> {
//                override fun onSuccess(data: ShortArray?) {
//                    LogUtils.d("readInputRegisters onSuccess " + Arrays.toString(data))
//                }
//
//                override fun onFailed(msg: String) {
//                    LogUtils.d("readInputRegisters onFailed $msg")
//                }
//            }, 1, 2, 8)
//        }
//        binding.btnWriteCoil.clickNoRepeat {
//            ModbusReq.getInstance().writeCoil(object : OnRequestBack<String> {
//                override fun onSuccess(s: String) {
//                    LogUtils.d("writeCoil onSuccess $s")
//                }
//
//                override fun onFailed(msg: String) {
//                    LogUtils.d("writeCoil onFailed $msg")
//                }
//            }, 1, 1, true)
//        }
//        binding.btnWriteRegister.clickNoRepeat {
//            ModbusReq.getInstance().writeRegister(object : OnRequestBack<String> {
//                override fun onSuccess(s: String) {
//                    LogUtils.d("writeRegister onSuccess $s")
//                }
//
//                override fun onFailed(msg: String) {
//                    LogUtils.d("writeRegister onFailed $msg")
//                }
//            }, 1, 1, 234)
//        }
//        binding.btnWriteRegisters.clickNoRepeat {
//            ModbusReq.getInstance().writeRegisters(object : OnRequestBack<String> {
//                override fun onSuccess(s: String) {
//                    LogUtils.d("writeRegisters onSuccess $s")
//                }
//
//                override fun onFailed(msg: String) {
//                    LogUtils.d("writeRegisters onFailed $msg")
//                }
//            }, 1, 2, shortArrayOf(211, 52, 34))
//        }

    }


}