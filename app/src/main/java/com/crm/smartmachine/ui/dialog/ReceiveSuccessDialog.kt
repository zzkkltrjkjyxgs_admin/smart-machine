package com.crm.smartmachine.ui.dialog

import android.content.Context
import android.os.CountDownTimer
import android.view.Gravity
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import com.cheng.library.base.BaseDialog
import com.cheng.library.common.clickNoRepeat
import com.crm.smartmachine.R
import com.crm.smartmachine.contants.ActionTypeEnum

class ReceiveSuccessDialog {
    class Builder(context: Context?) : BaseDialog.Builder<Builder>(context) {

        private var listener: OnButtonClickListener?=null
        private var ivClose: AppCompatImageView
        private var tvBackHome: AppCompatTextView
        private var tvContinue: AppCompatTextView
//        private var countDownTimer: CountDownTimer?=null

        init {
            setContentView(R.layout.dialog_receive_success)
            setAnimStyle(BaseDialog.ANIM_IOS)
            setGravity(Gravity.CENTER)
            setCancelable(false)
            setCanceledOnTouchOutside(false)

            ivClose = findViewById(R.id.iv_close)
            tvBackHome = findViewById(R.id.tvBackHome)
            tvContinue = findViewById(R.id.tvContinue)

            ivClose.clickNoRepeat {
                dismiss()
            }
            tvBackHome.clickNoRepeat {
                dismiss()
                listener?.onBackClick()
            }
            tvContinue.clickNoRepeat {
                dismiss()
                listener?.onContinueClick()
            }

//            countDownTimer = object : CountDownTimer(11000, 1000) {
//                override fun onTick(millisUntilFinished: Long) {
//                    tvBackHome.text = "返回首页(${millisUntilFinished / 1000}s)"
//                }
//
//                override fun onFinish() {
//                    tvBackHome.text = "返回首页"
//                    listener?.onBackClick()
//                }
//            }
//            postDelayed({
//                countDownTimer?.start()
//            }, 800)
        }

        fun setListener(listener: OnButtonClickListener):Builder{
            this.listener = listener
            return this
        }

//        override fun dismiss() {
//            countDownTimer?.cancel()
//            countDownTimer = null
//            super.dismiss()
//        }
    }

    interface OnButtonClickListener{
        fun onBackClick()
        fun onContinueClick()
    }
}