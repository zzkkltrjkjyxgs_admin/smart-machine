package com.crm.smartmachine

import com.cheng.library.BaseApplication
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MyApplication : BaseApplication() {
    companion object {
        lateinit var instance: MyApplication
    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        startKoin {
            androidContext(instance)
            modules(listOf(appModule))
        }
    }
}