package com.crm.smartmachine.api

import com.crm.smartmachine.bean.request.*
import com.crm.smartmachine.bean.response.*
import com.crm.smartmachine.network.HttpResult
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query

interface Api {

    /**
     * 获取货到id列表
     */
    @POST("/xzsw/lyj.action")
    suspend fun getLYJIDList(
        @Body request: LYJIDRequest,
        @Query("method") method: String = "getHd"
    ): HttpResult<List<LYJBean>>

    /**
     * 存货员登录
     */
    @POST("/xzsw/lyj.action")
    suspend fun bhyMemberLogin(
        @Query("method") method: String = "bhylogin",
        @Body request: LoginRequest
    ): HttpResult<String>

    /**
     * 获取货道列表
     */
    @POST("/xzsw/lyj.action")
    suspend fun getCaseList(
        @Query("method") method: String,
        @Body request: LYJIDRequest
    ): HttpResult<List<LYJCaseBean>>

    /**
     * 获取物品列表
     */
    @POST("/xzsw/lyj.action")
    suspend fun getGoodsList(
        @Query("method") method: String = "getwp"
    ): HttpResult<List<GoodsBean>>

    /**
     * 存货
     */
    @POST("/xzsw/lyj.action")
    suspend fun saveGoods(
        @Query("method") method: String = "ch",
        @Body request: List<SaveGoodsRequest>
    ): HttpResult<String>

    /**
     * 补货
     */
    @POST("/xzsw/lyj.action")
    suspend fun supplyGoods(
        @Body request: List<SupplyGoodsRequest>,
        @Query("method") method: String = "bh"
    ): HttpResult<String>

    /**
     * 报修
     */
    @POST("/xzsw/lyj.action")
    suspend fun repair(
        @Query("method") method: String = "bx",
        @Body request: BxRequest
    ): HttpResult<String>

    /**
     * 根据审批获取货到id列表
     */
    @POST("/xzsw/lyj.action")
    suspend fun getLQDList(
        @Query("method") method: String = "getlywp",
        @Body request: LQDListRequest
    ): HttpResult<List<ReceiveBillBean>>

    /**
     * 获取物品后修改后台数据
     */
    @POST("/xzsw/lyj.action")
    suspend fun updateGoodsInfo(
        @Body request: GoodsInfoRequest,
        @Query("method") method: String = "updatehd"
    ): HttpResult<String>


//    -----------------------0720接口-------------------------

    /**
     * 接口1：获取需要出货的审批单
     */
    @POST("/xzsw/lyj.action")
    suspend fun getChd(
        @Query("method") method: String = "getChd"
    ): HttpResult<CHDBean>

    /**
     * 接口2：领用机出货完毕，通知申请人取货
     */
    @POST("/xzsw/lyj.action")
    suspend fun chwb(
        @Body request: ChwbRequest,
        @Query("method") method: String = "chwb"
    ): HttpResult<String>

    /**
     * 接口3：根据取货码，调用后台，返回取货物品信息，信息包括取货位置：出货柜（默认）/储物柜编号。
     */
    @POST("/xzsw/lyj.action")
    suspend fun getSpdByQhm(
        @Body request: QhmRequest,
        @Query("method") method: String = "getSpdByQhm"
    ): HttpResult<QhBean>

    /**
     * 接口4：根据取货码，更新审批单或储物柜信息
     */
    @POST("/xzsw/lyj.action")
    suspend fun updateSpdByQhm(
        @Body request: QhmRequest,
        @Query("method") method: String = "updateSpdByQhm"
    ): HttpResult<String>

    /**
     * 接口5：补货员登录
     */
    @POST("/xzsw/lyj.action")
    suspend fun bhylogin(
        @Body request: LoginRequest,
        @Query("method") method: String = "bhylogin"
    ): HttpResult<String>

    /**
     * 接口6：获取所有未领取的审批单以及审批单下的物品信息
     */
    @POST("/xzsw/lyj.action")
    suspend fun getSpd(
        @Query("method") method: String = "getSpd"
    ): HttpResult<CHDBean>

    /**
     * 接口7：获取需要领取的物品信息--根据审批单unid
     */
    @POST("/xzsw/lyj.action")
    suspend fun getlywp(
        @Body request: SpdidRequest,
        @Query("method") method: String = "getlywp"
    ): HttpResult<List<WPBean>>

    /**
     * 接口8：获取存货柜--根据领用机id
     */
    @POST("/xzsw/lyj.action")
    suspend fun getGm(
        @Body request: LYJIDRequest,
        @Query("method") method: String = "getGm"
    ): HttpResult<List<HdBean>>

    /**
     * 接口9：获取货道--根据领用机id
     */
    @POST("/xzsw/lyj.action")
    suspend fun getHd(
        @Body request: LYJIDRequest,
        @Query("method") method: String = "getHd"
    ): HttpResult<List<HdBean>>

    /**
     * 接口10：获取物品
     */
    @POST("/xzsw/lyj.action")
    suspend fun getWp(
        @Body request: WpmcRequest,
        @Query("method") method: String = "getwp"
    ): HttpResult<List<WPBean>>

    /**
     * 接口11：补货接口
     */
    @POST("/xzsw/lyj.action")
    suspend fun bh(
        @Body request: List<BhRequest>,
        @Query("method") method: String = "bh"
    ): HttpResult<String>

    /**
     * 接口12：报修记录
     */
    @POST("/xzsw/lyj.action")
    suspend fun bx(
        @Body request: BxRequest,
        @Query("method") method: String = "bx"
    ): HttpResult<String>

    /**
     * 接口13：获取所有不同状态下的的审批单以及审批单下的物品信息
     */
    @POST("/xzsw/lyj.action")
    suspend fun getSpdBySpdzt(
        @Body request: SpdztRequest,
        @Query("method") method: String = "getSpdBySpdzt"
    ): HttpResult<List<CHDBean>>

    /**
     * 接口14：更新审批单领取物品位置
     */
    @POST("/xzsw/lyj.action")
    suspend fun updateSpdLqwpWz(
        @Body request: UpdateSpdLqwpWzRequest,
        @Query("method") method: String = "updateSpdLqwpWz"
    ): HttpResult<String>

}