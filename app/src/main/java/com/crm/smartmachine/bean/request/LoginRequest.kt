package com.crm.smartmachine.bean.request

data class LoginRequest(
    val loginid: String,
    val loginpwd: String
)
