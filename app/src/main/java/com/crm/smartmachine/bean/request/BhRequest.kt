package com.crm.smartmachine.bean.request

data class BhRequest(
    val wpwz: String, //0：货道/1：储物柜
    val hdbh: String, //货道编号
    val wpid: String, //物品id
    val wpbh: String, //物品编号
    val wpmc: String, //物品名称
    val bhsl: Int, //补货数量
    val type: String //1：增加 2：减少
)
