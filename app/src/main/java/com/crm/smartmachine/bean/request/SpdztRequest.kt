package com.crm.smartmachine.bean.request

data class SpdztRequest(
    val spdzt:String, //审批单状态:DQH(待取货)/YLQ(已领取)
    val curDay:String //是否只查询当前天：Y（当前天,最多100条）/N（最近100条）
)
