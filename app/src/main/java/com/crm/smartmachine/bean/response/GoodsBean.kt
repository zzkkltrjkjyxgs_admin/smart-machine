package com.crm.smartmachine.bean.response

data class GoodsBean(
    val unid: String,
    val wpmc: String, //物品名称
    val ggxh: String, //规格型号
    val wpbh: String, //物品编号
    var count: Int = 0 //数量
)