package com.crm.smartmachine.bean.request

data class ChwbRequest(
    val spdid: String, //审批单ID
    val wpwz: String, //物品位置，如果是在出货后，则传递0，在储物柜中，则传递柜门编号（0：出货口/柜门编号）
    val flag:String, //是否出货成功，Y出货成功，N出货失败
    val chms:String, //出货描述
    val ychwp: List<YCHWPRequest> //已出货物品，根据此信息，更新系统货道库存。
)