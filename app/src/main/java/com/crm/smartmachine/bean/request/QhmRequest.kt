package com.crm.smartmachine.bean.request

data class QhmRequest(
    val qhm: String, //取货码
    val isAdmin: String? = null //取货时N,存货时Y
)