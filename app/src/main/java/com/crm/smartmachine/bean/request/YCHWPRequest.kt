package com.crm.smartmachine.bean.request

data class YCHWPRequest(
    val hdbh: String, //货道编号
    val wpbh: String //物品编号
)
