package com.crm.smartmachine.bean.response

data class LYJBean(
    val unid: String,
    val mc: String?,
    val kcsl: String?,
    val bh: String
)