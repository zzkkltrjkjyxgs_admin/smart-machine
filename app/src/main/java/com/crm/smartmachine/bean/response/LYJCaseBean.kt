package com.crm.smartmachine.bean.response

data class LYJCaseBean(
    val unid: String,
    val mc: String?, //名称
    val kcsl: String?, //库存数量
    val bh: String //编号
)