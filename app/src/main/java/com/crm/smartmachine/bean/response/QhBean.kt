package com.crm.smartmachine.bean.response

data class QhBean(
    val unid: String,
    val djbh: String,
    val lyr: String,
    val qhm: String,
    val slwpList: List<SPDBean>,
    val wpwz: String
)