package com.crm.smartmachine.bean.response

data class TestHdBean(
    val hdbh: String, //货道编号
    val wpbh: String?, //物品编号
    val kcsl: String?//库存数量
)