package com.crm.smartmachine.bean.request

data class SaveGoodsRequest(
    val gmid: String, //柜道编号
    val wpmc: String, //物品名称
    val wpsl: String, //物品数量
    val type: String //1-存 2-取
)
