package com.crm.smartmachine.bean.response

data class WPBean(
    val unid: String,
    val kcsl: String, //库存
    var slsl: String,
    val wpmc: String, //物品名称
    val ggxh: String, //规格型号
    val wpbh: String, //物品编号

    //self
    var count: Int = 0, //数量
    var hdwz: String //货道位置
)