package com.crm.smartmachine.bean.request

data class GoodsInfoRequest (
    val spdid: String,
    val hdid: String,
    val wpid: String
)