package com.crm.smartmachine.bean.request

data class SupplyGoodsRequest(
    val hdid: String,
    val wpmc: String, //物品名称
    val bhsl: String //物品数量
)
