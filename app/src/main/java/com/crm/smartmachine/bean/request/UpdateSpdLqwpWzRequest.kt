package com.crm.smartmachine.bean.request

data class UpdateSpdLqwpWzRequest(
    val spdid:String, //审批单ID
    val wpwz:String //物品位置（0或空为出货口/其他储物柜编号）
)
