package com.crm.smartmachine.bean.response

/**
 * 出货单
 */
data class CHDBean(
    val unid: String, ////审批单id
    val djbh: String, //单据编号
    val lyr: String,
    val qhm: String, //取货码
    val slwpList: List<WPBean>, //物品列表
    val wpwz:String
//    val mc: String?, //名称
//    val wz: String?,
//    val kcsl: String?, //库存数量
//    val bh: String //货道编号
)