package com.crm.smartmachine.bean.response

data class HdBean(
    val unid: String,
    val mc: String?, //物品名称
    val wpbh: String?, //物品编号
    val wz: String?,
    val kcsl: String?, //库存数量
    val bh: String //货道编号
)