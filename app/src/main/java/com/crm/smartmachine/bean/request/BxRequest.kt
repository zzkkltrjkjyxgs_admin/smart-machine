package com.crm.smartmachine.bean.request

data class BxRequest(
    val lyjid: String, //
    val bxnr: String, //报修内容
    val bxr: String, //报修人
    val lxdh: String //联系电话
)
