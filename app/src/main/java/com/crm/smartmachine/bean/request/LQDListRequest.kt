package com.crm.smartmachine.bean.request

data class LQDListRequest(
    val spdid: String
)