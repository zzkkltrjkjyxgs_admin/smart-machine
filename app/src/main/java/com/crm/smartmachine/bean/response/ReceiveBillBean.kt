package com.crm.smartmachine.bean.response

data class ReceiveBillBean(
    val wpbh: String,
    val wpmc: String,
    val kcsl: Int,
    val slsl: Int,
    val parunid: String,
    val unid: String,
    val sfylq: String,
    val createunitname: String
)