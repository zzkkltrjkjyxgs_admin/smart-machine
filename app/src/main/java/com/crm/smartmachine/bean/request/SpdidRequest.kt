package com.crm.smartmachine.bean.request

data class SpdidRequest(
    val spdid: String
)