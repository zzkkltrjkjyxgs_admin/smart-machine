package com.crm.smartmachine.bean

/**
 * 货道里待出货物品
 */
data class DchWpBean(
    val hdbh: String, //货道编号
    val hdsl: Int, //货道里的数量
    val wpbh: String //物品编号
)
