package com.crm.smartmachine.bean.response

data class GoodsaaaBean(
    val goodsName: String,
    val goodsCount: Int
)