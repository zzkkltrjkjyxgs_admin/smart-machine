package com.crm.smartmachine.manager

import com.cheng.library.common.toast
import com.crm.smartmachine.contants.Constants
import com.crm.smartmachine.manager.callback.WriteRegistersCallback
import com.zgkxzx.modbus4And.requset.ModbusReq
import com.zgkxzx.modbus4And.requset.OnRequestBack

object ModbusManager {

    fun reset(callback: WriteRegistersCallback? = null) {
        ModbusReq.getInstance().writeRegisters(object : OnRequestBack<String> {
            override fun onSuccess(s: String) {
                callback?.onSuccess()
            }

            override fun onFailed(msg: String) {
                toast("onFailed:$msg")
            }
        }, Constants.SLAVEID, 0, shortArrayOf(0, 0, 0))
    }

    fun openCase4Save(caseBh: String, callback: WriteRegistersCallback) {
        ModbusReq.getInstance().writeRegisters(object : OnRequestBack<String> {
            override fun onSuccess(s: String) {
                callback.onSuccess()
            }

            override fun onFailed(msg: String) {
                toast("onFailed:$msg")
            }
        }, Constants.SLAVEID, 0, shortArrayOf(101, caseBh.toShort()))
    }

    fun openCase4SupplyStart(caseBh: String, callback: WriteRegistersCallback) {
        ModbusReq.getInstance().writeRegisters(object : OnRequestBack<String> {
            override fun onSuccess(s: String) {
                callback.onSuccess()
            }

            override fun onFailed(msg: String) {
                toast("onFailed:$msg")
            }
        }, Constants.SLAVEID, 0, shortArrayOf(102, caseBh.toShort()))
    }

    fun openCase4SupplyStop(caseBh: String, callback: WriteRegistersCallback) {
        ModbusReq.getInstance().writeRegisters(object : OnRequestBack<String> {
            override fun onSuccess(s: String) {
                callback.onSuccess()
            }

            override fun onFailed(msg: String) {
                toast("onFailed:$msg")
            }
        }, Constants.SLAVEID, 0, shortArrayOf(0, caseBh.toShort()))
    }

    fun openCase4Receive(wpbh: String, count: Int, callback: WriteRegistersCallback? = null) {
        ModbusReq.getInstance().writeRegisters(object : OnRequestBack<String> {
            override fun onSuccess(s: String) {
                callback?.onSuccess()
            }

            override fun onFailed(msg: String) {
                toast("onFailed:$msg")
            }
        }, Constants.SLAVEID, 0, shortArrayOf(104, wpbh.toShort(), count.toShort()))
//        }, Constants.SLAVEID, 0, shortArrayOf(100, wpbh.toShort(), count.toShort()))
    }

    fun openCase4Receive(callback: WriteRegistersCallback? = null) {
        ModbusReq.getInstance().writeRegisters(object : OnRequestBack<String> {
            override fun onSuccess(s: String) {
                callback?.onSuccess()
            }

            override fun onFailed(msg: String) {
                toast("onFailed:$msg")
            }
        }, Constants.SLAVEID, 0, shortArrayOf(104, 0, 0))
//        }, Constants.SLAVEID, 0, shortArrayOf(100, wpbh.toShort(), count.toShort()))
    }

    fun chuhuo(hdbh: String, count: String, callback: WriteRegistersCallback? = null) {
        ModbusReq.getInstance().writeRegisters(object : OnRequestBack<String> {
            override fun onSuccess(s: String) {
                callback?.onSuccess()
            }

            override fun onFailed(msg: String) {
                toast("onFailed:$msg")
            }
        }, Constants.SLAVEID, 0, shortArrayOf(100, hdbh.toShort(), count.toShort()))
    }

    fun observeChuhuo(callback: WriteRegistersCallback) {
        ModbusReq.getInstance().readHoldingRegisters(object : OnRequestBack<ShortArray?> {
            override fun onSuccess(data: ShortArray?) {
//                LogUtils.a(data)
                data?.let {
                    if (data.size > 2 && data[0].toInt() == 0 && data[1].toInt() == 0 && data[2].toInt() == 0) {
                        callback.onSuccess()
                    }
                }
            }

            override fun onFailed(msg: String) {
                toast("监听出货指令error:$msg")
            }
        }, Constants.SLAVEID, 0, 8)
    }
}