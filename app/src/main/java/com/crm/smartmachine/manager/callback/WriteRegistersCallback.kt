package com.crm.smartmachine.manager.callback

interface WriteRegistersCallback {
    fun onSuccess()
}