package com.crm.smartmachine.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.crm.smartmachine.api.Api
import com.crm.smartmachine.bean.request.*
import com.crm.smartmachine.bean.response.*
import com.crm.smartmachine.contants.Constants
import com.crm.smartmachine.contants.EventConstants
import com.crm.smartmachine.event.Message
import com.crm.smartmachine.network.BaseViewModel
import com.crm.smartmachine.repository.GlobalRepository

class MainViewModel(private val api: Api, repo: GlobalRepository) : BaseViewModel(repo) {

    /**
     * 按钮输入事件发送广播给当前获取焦点的输入框
     */
    fun setInput(c: String) {
        onInput(c)
    }


    val caseListResponse: LiveData<List<HdBean>>
        get() = _caseListResponse
    private val _caseListResponse = MutableLiveData<List<HdBean>>()

    /**
     * 接口8：获取存货柜--根据领用机id
     * 获取存/取货货道列表
     */
    fun getSaveCaseList(lyjid: String) {
        launch {
            val res = api.getGm(LYJIDRequest(lyjid))
            if (res.isSuccess()) {
                _caseListResponse.postValue(res.data)
            }
        }
    }

    /**
     * 接口9：获取补货货道列表
     */
    fun getSupplyCaseList() {
        launch {
            val res = api.getHd(LYJIDRequest(Constants.LYJID))
            if (res.isSuccess()) {
                _caseListResponse.postValue(res.data)
            }
        }
    }


    val saveGoodsResponse: LiveData<String>
        get() = _saveGoodsResponse
    private val _saveGoodsResponse = MutableLiveData<String>()

    /**
     * 存货
     */
    fun saveGoods(list: List<SaveGoodsRequest>) {
        launch {
            val res = api.saveGoods(request = list)
            if (res.isSuccess()) {
                defUI.msgEvent.postValue(Message(0, EventConstants.UPDATE_HOME))
                getSaveCaseList(Constants.LYJID)
                _saveGoodsResponse.postValue(res.msg)
            }
        }
    }

    val bhResponse: LiveData<String>
        get() = _bhResponse
    private val _bhResponse = MutableLiveData<String>()


    val updateGoodsInfoResponse: LiveData<String>
        get() = _updateGoodsInfoResponse
    private val _updateGoodsInfoResponse = MutableLiveData<String>()

    /**
     * 获取物品后修改后台数据
     */
    fun updateGoodsInfo(spdid: String, hdid: String, wpid: String) {
        launch {
            val res = api.updateGoodsInfo(GoodsInfoRequest(spdid, hdid, wpid))
            if (res.isSuccess()) {
                defUI.msgEvent.postValue(Message(0, EventConstants.UPDATE_HOME))
                _updateGoodsInfoResponse.postValue(res.msg)
            }
        }
    }

    //-----------------------------------

    val getChdResponse: LiveData<CHDBean>
        get() = _getChdResponse
    private val _getChdResponse = MutableLiveData<CHDBean>()
    val getChdErrorResponse = MutableLiveData<String>()

    /**
     * 接口1：获取需要出货的审批单
     */
    fun getChd() {
        launchGo({
            val res = api.getChd()
            if (res.isSuccess())
                _getChdResponse.postValue(res.data)
        }, {
            getChdErrorResponse.postValue(it.errMsg)
        })
    }

    val chwbResponse: LiveData<String>
        get() = _chwbResponse
    private val _chwbResponse = MutableLiveData<String>()

    /**
     * 接口2：领用机出货完毕，通知申请人取货
     * @param spdid 审批单ID
     * @param wpwz 物品位置，如果是在出货后，则传递0，在储物柜中，则传递柜门编号（0：出货口/柜门编号）
     * @param ychwp 已出货物品，根据此信息，更新系统货道库存。
     */
    fun chwb(requestBean: ChwbRequest) {
        launch {
            val res = api.chwb(requestBean)
            if (res.isSuccess())
                _chwbResponse.postValue(res.msg)
        }
    }

    /**
     * 接口2，出货失败
     */
    fun chError(requestBean: ChwbRequest) {
        launch {
            val res = api.chwb(requestBean)
            if (res.isSuccess())
                _chwbResponse.postValue(res.msg)
        }
    }

    val getSpdByQhmResponse: LiveData<QhBean>
        get() = _getSpdByQhmResponse
    private val _getSpdByQhmResponse = MutableLiveData<QhBean>()

    /**
     * 接口3：根据取货码，调用后台，返回取货物品信息，信息包括取货位置：出货柜（默认）/储物柜编号。
     */
    fun getSpdByQhm(qhm: String, isAdmin: String) {
        launch {
            val res = api.getSpdByQhm(QhmRequest(qhm, isAdmin))
            if (res.isSuccess())
                _getSpdByQhmResponse.postValue(res.data)
        }
    }

    val updateSpdByQhmResponse: LiveData<String>
        get() = _updateSpdByQhmResponse
    private val _updateSpdByQhmResponse = MutableLiveData<String>()

    /**
     * 接口4：根据取货码，更新审批单或储物柜信息
     */
    fun updateSpdByQhm(qhm: String) {
        launch {
            val res = api.updateSpdByQhm(QhmRequest(qhm))
            if (res.isSuccess())
                _updateSpdByQhmResponse.postValue(res.msg)
        }
    }

    val bhyloginResponse: LiveData<String>
        get() = _bhyloginResponse
    private val _bhyloginResponse = MutableLiveData<String>()

    /**
     * 接口5：补货员登录
     */
    fun bhylogin(loginid: String, loginpwd: String) {
        launch {
            val res = api.bhylogin(LoginRequest(loginid, loginpwd))
            if (res.isSuccess()) {
                _bhyloginResponse.postValue(res.msg)
            }
        }
    }

    val getSpdResponse: LiveData<CHDBean>
        get() = _getSpdResponse
    private val _getSpdResponse = MutableLiveData<CHDBean>()

    /**
     * 接口6：获取所有未领取的审批单以及审批单下的物品信息
     */
    fun getSpd() {
        launch {
            val res = api.getSpd()
            if (res.isSuccess())
                _getSpdResponse.postValue(res.data)
        }
    }

    val getlywpResponse: LiveData<List<WPBean>>
        get() = _getlywpResponse
    private val _getlywpResponse = MutableLiveData<List<WPBean>>()

    /**
     * 接口7：获取需要领取的物品信息--根据审批单unid
     */
    fun getlywp(spdid: String) {
        launch {
            val res = api.getlywp(SpdidRequest(spdid))
            if (res.isSuccess()) {
                _getlywpResponse.postValue(res.data)
            }
        }
    }

    val getHdResponse: LiveData<List<HdBean>>
        get() = _getHdResponse
    private val _getHdResponse = MutableLiveData<List<HdBean>>()

    /**
     * 接口9：获取货道--根据领用机id
     */
    fun getHd() {
        launch {
            val res = api.getHd(LYJIDRequest(Constants.LYJID))
            if (res.isSuccess()) {
                _getHdResponse.postValue(res.data)
            }
        }
    }

    val getWpResponse: LiveData<List<WPBean>>
        get() = _getWpResponse
    private val _getWpResponse = MutableLiveData<List<WPBean>>()

    /**
     * 接口10：获取物品
     */
    fun getWp(wpmc: String? = null) {
        launch {
            val res = api.getWp(WpmcRequest())
            if (res.isSuccess()) {
                _getWpResponse.postValue(res.data)
            }
        }
    }

    /**
     * 接口11：补货接口
     */
    fun bh(requestBean: BhRequest) {
        launch {
            val res = api.bh(arrayListOf(requestBean))
            if (res.isSuccess()) {
                defUI.msgEvent.postValue(Message(0, EventConstants.UPDATE_HOME))
                _bhResponse.postValue(res.msg)
            }
        }
    }

    val bxResponse: LiveData<String>
        get() = _bxResponse
    private val _bxResponse = MutableLiveData<String>()

    /**
     * 接口12：报修记录
     */
    fun bx(requestBean: BxRequest) {
        launch {
            val res = api.bx(requestBean)
            if (res.isSuccess())
                _bxResponse.postValue(res.msg)
        }
    }

    val getSpdBySpdztResponse: LiveData<List<CHDBean>>
        get() = _getSpdBySpdztResponse
    private val _getSpdBySpdztResponse = MutableLiveData<List<CHDBean>>()

    /**
     * 接口13：获取所有不同状态下的的审批单以及审批单下的物品信息
     */
    fun getSpdBySpdzt(requestBean: SpdztRequest) {
        launch {
            val res = api.getSpdBySpdzt(requestBean)
            if (res.isSuccess())
                _getSpdBySpdztResponse.postValue(res.data)
        }
    }

    val updateSpdLqwpWzResponse: LiveData<String>
        get() = _updateSpdLqwpWzResponse
    private val _updateSpdLqwpWzResponse = MutableLiveData<String>()

    /**
     * 接口14：更新审批单领取物品位置
     */
    fun updateSpdLqwpWz(requestBean: UpdateSpdLqwpWzRequest) {
        launch {
            val res = api.updateSpdLqwpWz(requestBean)
            if (res.isSuccess())
                _updateSpdLqwpWzResponse.postValue(res.msg)
        }
    }
}